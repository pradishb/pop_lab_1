C		FIND AM, GM, HM
		PROGRAM ABC
		REAL X,Y,G,AM,GN,HM
		WRITE (*,*)'Enter x, y, z'
		READ *,X,Y,Z 						!READ VALUES
		AM = (X + Y + Z) / 3.0				!CALCULATE AM
		GM = (X * Y * Z) ** (1.0 / 3.0)		!CALCULATE GM
		HM = 3 / ( 1 / X + 1 / Y + 1 / Z)	!CALCULATE HM
		WRITE (*,*)AM,GM,HM					!PRINT MEANS
		END